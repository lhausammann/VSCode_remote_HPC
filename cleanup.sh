#!/bin/bash

if [[ $# -lt 1 ]]
then
    echo -e "Error: No ETH username is specified, terminating script\n"
    exit 1
fi

VSC_USERNAME=$1

VSC_TUNNEL=$(cat reconnect_info | grep -o -E '[0-9]+:([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+):[0-9]+')
TUNNEL_JOBS=$(ps -u | grep $VSC_TUNNEL | grep ssh | awk '{ print $2 }')

for TUNNEL_JOB in $TUNNEL_JOBS; do echo $TUNNEL_JOB; kill $TUNNEL_JOB; done

ssh -T $VSC_USERNAME@euler.ethz.ch bkill $(cat reconnect_info  | grep BJOB | awk '{print $NF}') 
